#**README  ~**#

This is the repository for my Sudoku Game created for UBHacking 2015, a 24 hour hackathon hosted at the State University at Buffalo.  This was created in the fall of my freshman year in college.  After UBHacking 2015, I have done nothing to update the project and fix minor bugs.

## Contact  ~ ##
* Email    - kurtlasc@gmail.com
* LinkedIn - https://www.linkedin.com/in/kurtlaschinger