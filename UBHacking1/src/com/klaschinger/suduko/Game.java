package com.klaschinger.suduko;

import java.util.ArrayList;
import java.util.Random;

public class Game {
	
	private ArrayList<int[]> _list = new ArrayList<int[]>();
	
	public Game(){
		int[] list1 = {1,2,3,4,5,6,7,8,9};
		int[] list2 = {7,8,9,1,2,3,4,5,6};
		int[] list3 = {4,5,6,7,8,9,1,2,3};
		int[] list4 = {9,1,2,3,4,5,6,7,8};
		int[] list5 = {6,7,8,9,1,2,3,4,5};
		int[] list6 = {3,4,5,6,7,8,9,1,2};
		int[] list7 = {8,9,1,2,3,4,5,6,7};
		int[] list8 = {5,6,7,8,9,1,2,3,4};
		int[] list9 = {2,3,4,5,6,7,8,9,1};
		
		_list.add(list1);
		_list.add(list2);
		_list.add(list3);
		_list.add(list4);
		_list.add(list5);
		_list.add(list6);
		_list.add(list7);
		_list.add(list8);
		_list.add(list9);
		
		Random rand = new Random();
		
		for(int i = 0; i < 100; i++){
			
			int changeFrom = rand.nextInt(9) + 1;
			int changeTo = rand.nextInt(9) + 1;
			
			for(int j = 0; j < 9; j++){
				int[] array = _list.get(j);
				
				for (int k = 0; k < 9; k++){
					
					if (array[k] == changeFrom){
						array[k] = changeTo;
						_list.set(j, array);
					} else if (array[k] == changeTo){
						array[k] = changeFrom;
						_list.set(j, array);
					}
				}
			}
		}
		
		int goneTiles = 0;
		
		while (Sudoku.getDifficulty() > goneTiles){
			ArrayList<int[]> modified = new ArrayList<int[]>();;
			
			for (int i = 0; i < 9; i++){
				modified.add(_list.get(i));
			}
			
			int x = rand.nextInt(9);
			int y = rand.nextInt(9);
			
			int[] array = modified.get(y);
			if (array[x] != 0){
				array[x] = 0;
				modified.set(y, array);
				
				boolean isValid = false;
			
				ArrayList<Number> numbers = new ArrayList<Number>();
				ArrayList<Integer> normal = new ArrayList<Integer>();
				for (int k = 1; k < 10; k++){
					normal.add(k);
				}
			
			
				for(int i = 0; i < 9; i++){
					int[] modList = modified.get(i);
				
					for (int j = 0; j < 9; j++){
					
						if (modList[j] == 0){
							ArrayList<Integer> nums = new ArrayList<Integer>();
							nums.addAll(normal);
						
							//row
							for (int num: modList){
								if(num != 0 && nums.contains(num)){
									nums.remove(nums.indexOf(num));
								}
							}
						
							//column
							for (int k = 0; k < 9; k++){
								int[] colArray = modified.get(k);
								if(colArray[j] != 0 && nums.contains(colArray[j])){
									nums.remove(nums.indexOf(colArray[j]));
								}
							}
						
							//square
							for (int k = 0; k < 3; k++){
								for (int l = 0; l < 3; l++){
									int[] boxArray = modified.get(k + (i/3) * 3);
								
									if (boxArray[l + (j/3) * 3] != 0 && nums.contains(boxArray[l + (j/3) * 3])){
										nums.remove(nums.indexOf(boxArray[(l + (j/3) * 3)]));
									}
								}
							}
							
							numbers.add(new Number(j,i,nums));
						}
					}
				}
				
				ArrayList<Number> testedNums = new ArrayList<Number>();
				
				ArrayList<Number> testNums = new ArrayList<Number>();
				
				//row
				for (int i = 0; i < 9; i++){
					testNums.clear();
					
					for (Number num: numbers){
						if (num.getRow() == i){
							testNums.add(num);
						}
					}
					
					for(Number num: testNums){
						ArrayList<Integer> valid = new ArrayList<Integer>();
						ArrayList<Integer> removal = new ArrayList<Integer>();
						valid.addAll(num.getFree());
						
						for (Number num2: testNums){
							if (!num.equals(num2)){
								for (int freeNum: valid){
									if (num2.getFree().contains(freeNum)){
										removal.add(freeNum);
									}
								}
								for (int e: removal){
									valid.remove((Integer) e);
								}
								
								removal.clear();
							}
						}
						
						boolean remove = false;
						Number removeNum = null;
						
						for (Number checkNum: testedNums){
							if (checkNum.getRow() == num.getRow() && checkNum.getCol() == num.getCol()){
								remove = true;
								removeNum = checkNum;
							}
						}
						
						if (remove){
							for (int freeNum: valid){
								if (removeNum.getFree().contains(freeNum)){
									removal.add(freeNum);
								}
							}
							
							for (int e: removal){
								valid.remove((Integer) e);
							}
							
							testedNums.remove(removeNum);
						}
						
						Number finalNum = new Number(num.getRow(), num.getCol(), valid);
						testedNums.add(finalNum);
					}
				}
				
				for (Number num: testedNums){
					if (num.getFree().size() == 1){
						isValid = true;
					}
				}
				
				testedNums.clear();
				/*
				//col
				for (int i = 0; i < 9; i++){
					testNums.clear();
					
					for (Number num: numbers){
						if (num.getCol() == i){
							testNums.add(num);
						}
					}
					
					for(Number num: testNums){
						ArrayList<Integer> valid = new ArrayList<Integer>();
						ArrayList<Integer> removal = new ArrayList<Integer>();
						valid.addAll(num.getFree());
						
						for (Number num2: testNums){
							if (!num.equals(num2)){
								for (int freeNum: valid){
									if (num2.getFree().contains(freeNum)){
										removal.add(freeNum);
									}
								}
								for (int e: removal){
									valid.remove((Integer) e);
								}
								
								removal.clear();
							}
						}
						
						boolean remove = false;
						Number removeNum = null;
						
						for (Number checkNum: testedNums){
							if (checkNum.getRow() == num.getRow() && checkNum.getCol() == num.getCol()){
								remove = true;
								removeNum = checkNum;
							}
						}
						
						if (remove){
							for (int freeNum: valid){
								if (removeNum.getFree().contains(freeNum)){
									removal.add(freeNum);
								}
							}
							
							for (int e: removal){
								valid.remove((Integer) e);
							}
							
							testedNums.remove(removeNum);
						}
						
						Number finalNum = new Number(num.getRow(), num.getCol(), valid);
						testedNums.add(finalNum);
					}
				}
				
				for (Number num: testedNums){
					if (num.getFree().size() == 1){
						isValid = true;
					}
				}
				
				testedNums.clear();
				
				//box
				for (int i = 0; i < 9; i++){
					testNums.clear();
					
					for (Number num: numbers){
						if (num.getBox() == i){
							testNums.add(num);
						}
					}
					
					for(Number num: testNums){
						ArrayList<Integer> valid = new ArrayList<Integer>();
						ArrayList<Integer> removal = new ArrayList<Integer>();
						valid.addAll(num.getFree());
						
						for (Number num2: testNums){
							if (!num.equals(num2)){
								for (int freeNum: valid){
									if (num2.getFree().contains(freeNum)){
										removal.add(freeNum);
									}
								}
								for (int e: removal){
									valid.remove((Integer) e);
								}
								
								removal.clear();
							}
						}
						
						boolean remove = false;
						Number removeNum = null;
						
						for (Number checkNum: testedNums){
							if (checkNum.getRow() == num.getRow() && checkNum.getCol() == num.getCol()){
								remove = true;
								removeNum = checkNum;
							}
						}
						
						if (remove){
							for (int freeNum: valid){
								if (removeNum.getFree().contains(freeNum)){
									removal.add(freeNum);
								}
							}
							
							for (int e: removal){
								valid.remove((Integer) e);
							}
							
							testedNums.remove(removeNum);
						}
						
						Number finalNum = new Number(num.getRow(), num.getCol(), valid);
						testedNums.add(finalNum);
					}
				}
				*/
				
				for (Number num: testedNums){
					if (num.getFree().size() == 1){
						isValid = true;
					}
				}
			
				if(isValid){
					goneTiles++;
					_list = modified;
				}
			}
		}
		
	}
	
	public String checkGame(){
		
		for (int[] array: _list){
			for (int i: array){
				if (i == 0){
					return "Game not Finished";
				}
			}
		}
		
		for (int[] array: _list){
			for (int i = 0; i < 9; i++){
				for (int j = 0; j < 9; j++){
					if (array[i] == array[j] && i != j){
						return "You have a mistake in your board";
					}
				}
			}
		}
		
		for (int i = 0; i < 9; i++){
			for (int j = 0; j < 9; j++){
				for (int k = 0; k < 9; k++){
					
					int[] array1 = _list.get(j);
					int[] array2 = _list.get(k);
				
					if (array1[i] == array2[i] && j != k){
						return "You have a mistake in your board";
					}
				}
			}
		}
		
		for (int i = 0; i < 3; i++){
			for (int j = 0; j < 3; j++){
				for (int k = 0; k < 3; k++){
					for (int l = 0; l < 3; l++){
						for (int m = 0; m < 3; m++){
							for (int n = 0; n < 3; n++){
								
								int[] array1 = _list.get(k + i*3);
								int[] array2 = _list.get(l + i*3);
				
								if (array1[m + j*3] == array2[n + j*3] && k != l && m != n){
									return "You have a mistake in your board";
								}
								
							}
						}
					}
				}
			}
		}
		
		return "Congratulations!!!  You have solved it";
		
	}
	
	public ArrayList<int[]> getBoard(){
		return _list;
	}

	public void changeNum(int _x, int _y, String selectedItem) {
		
		int[] array = _list.get(_y);
		array[_x] = Integer.valueOf(selectedItem);
		_list.set(_y, array);
		
	}

}
