package com.klaschinger.suduko;

import java.util.ArrayList;

public class Number {
	
	private int _row;
	private int _col;
	private int _box;
	private ArrayList<Integer> _free = new ArrayList<Integer>();
	
	public Number(int x, int y, ArrayList<Integer> free) {
		_row = x;
		_col = y;
		_free = free;
		
		_box = x/3 + (y/3) * 3;
	}
	
	public void removeNumber(int i){
		_free.remove((Integer) i);
	}
	
	public int getRow(){
		return _row;
	}
	
	public int getCol(){
		return _col;
	}
	
	public int getBox(){
		return _box;
	}
	
	public ArrayList<Integer> getFree(){
		return _free;
	}

}
