package com.klaschinger.suduko;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GraphicsPanel extends JPanel {
	
	private Image _image;
	
	public GraphicsPanel(Image image){
		this._image = image;
	}
	
	public void paintComponent(Graphics g){
		
		if (_image != null){
			g.drawImage(_image.getScaledInstance(this.getWidth(), this.getHeight() - 30, Image.SCALE_SMOOTH), 0, 0, null);
		}
		
	}

}
