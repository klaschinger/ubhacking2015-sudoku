package com.klaschinger.suduko;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextArea;

public class CheckGameEvent implements ActionListener {

	private Game _game;
	private JTextArea _result;
	private JFrame _frame;
	
	public CheckGameEvent(Game game, JTextArea result, JFrame frame){
		_game = game;
		_result = result;
		_frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		_result.setText(_game.checkGame());
		_frame.revalidate();
	}

}
