package com.klaschinger.suduko;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class NewGameEvent implements ActionListener {
	
	private JPanel _panel;
	private JFrame _frame;

	public NewGameEvent(JPanel panel, JFrame frame) {
		_panel = panel;
		_frame = frame;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		Sudoku.setupGame(_panel, new Game(), _frame);

	}

}
