package com.klaschinger.suduko;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

public class Sudoku implements Runnable {
	
	private static int _difficulty = 40;
	private Image _icon;
	private Image _logo;
	private Image _settingsBackground;
	private Image _gameBackground;
	
	@Override
	public void run() {
		Game game = new Game();
		loadImages();
		
		JFrame frame = new JFrame("Sudoku");
		JPanel mainPanel = new JPanel();
		GraphicsPanel settingsPanel = new GraphicsPanel(_settingsBackground);
		GraphicsPanel gamePanel = new GraphicsPanel(_gameBackground);
		GraphicsPanel gameLogo = new GraphicsPanel(_logo);
		JTextArea instructions = new JTextArea();
		JButton check = new JButton("Check");
		JTextArea results = new JTextArea();
		JButton newGame = new JButton("New Game");
		JRadioButton easy = new JRadioButton("Easy", true);
		JRadioButton medium = new JRadioButton("Medium");
		JRadioButton hard = new JRadioButton("Hard");
		ButtonGroup difficulty = new ButtonGroup();
		difficulty.add(easy);
		difficulty.add(medium);
		difficulty.add(hard);
		
		frame.setMinimumSize(new Dimension(800, 600));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(mainPanel);
		frame.setIconImage(_icon);
		frame.setResizable(false);
		
		mainPanel.setLayout(null);
		mainPanel.add(settingsPanel);
		mainPanel.add(gamePanel);
		
		settingsPanel.setLayout(null);
		settingsPanel.setSize(new Dimension(frame.getWidth()/4, frame.getHeight()));
		settingsPanel.add(gameLogo);
		settingsPanel.add(instructions);
		settingsPanel.add(check);
		settingsPanel.add(results);
		settingsPanel.add(newGame);
		settingsPanel.add(easy);
		settingsPanel.add(medium);
		settingsPanel.add(hard);
		
		if ((frame.getWidth()/4)*3 > frame.getHeight()){
			gamePanel.setSize(new Dimension(frame.getHeight(), frame.getHeight()));
		} else {
			gamePanel.setSize(new Dimension((frame.getWidth()/4)*3, (frame.getWidth()/4)*3));
		}
		gamePanel.setLayout(null);
		gamePanel.setLocation(frame.getWidth()/4, 0);
		gamePanel.prepareImage(_gameBackground, null);
		setupGame(gamePanel, game, frame);
		
		gameLogo.setSize(settingsPanel.getWidth(), (int) (settingsPanel.getWidth()/1.5));
		gameLogo.setLocation(0, 0);
		
		instructions.setText("Sudoku is playerd by filling in blank tiles until you have one of every number in each box, column and row.");
		instructions.setFont(new Font("Times New Roman", Font.BOLD, 17));
		instructions.setOpaque(false);
		instructions.setSize(settingsPanel.getWidth() - 60, 150);
		instructions.setEditable(false);
		instructions.setLineWrap(true);
		instructions.setWrapStyleWord(true);
		instructions.setLocation(30, 100);
		
		check.setFont(new Font("Times New Roman", Font.PLAIN, 24));
		check.setSize(check.getPreferredSize());
		check.setLocation(settingsPanel.getWidth()/2 - check.getWidth()/2, gameLogo.getHeight() + instructions.getHeight() - 40);
		check.addActionListener(new CheckGameEvent(game, results, frame));
		
		results.setText("");
		results.setFont(new Font("Times New Roman", Font.BOLD, 20));
		results.setOpaque(false);
		results.setSize(settingsPanel.getWidth() - 60, 50);
		results.setEditable(false);
		results.setLineWrap(true);
		results.setWrapStyleWord(true);
		results.setLocation(30, gameLogo.getHeight() + instructions.getHeight() + check.getHeight() - 40);
		
		newGame.setFont(new Font("Times New Roman", Font.PLAIN, 24));
		newGame.setSize(newGame.getPreferredSize());
		newGame.setLocation(settingsPanel.getWidth()/2 - newGame.getWidth()/2, frame.getHeight() - 200);
		newGame.addActionListener(new NewGameEvent(gamePanel, frame));
		
		easy.setFont(new Font("Times New Roman", Font.BOLD, 16));
		easy.setOpaque(false);
		easy.setSize(easy.getPreferredSize());
		easy.setLocation(settingsPanel.getWidth()/2 - easy.getWidth()/2, frame.getHeight() - 150);
		easy.addActionListener(new DifficultyEvent());
		
		medium.setFont(new Font("Times New Roman", Font.BOLD, 16));
		medium.setOpaque(false);
		medium.setSize(medium.getPreferredSize());
		medium.setLocation(settingsPanel.getWidth()/2 - medium.getWidth()/2, frame.getHeight() - 120);
		medium.addActionListener(new DifficultyEvent());
		
		hard.setFont(new Font("Times New Roman", Font.BOLD, 16));
		hard.setOpaque(false);
		hard.setSize(hard.getPreferredSize());
		hard.setLocation(settingsPanel.getWidth()/2 - hard.getWidth()/2, frame.getHeight() - 90);
		hard.addActionListener(new DifficultyEvent());
		
		frame.pack();
		frame.setVisible(true);
		
	}
	
	public static void setupGame(JPanel panel, Game game, JFrame frame) {
		panel.removeAll();
		
		int w = (panel.getWidth()-5)/9;
		int h = (panel.getHeight()-9)/9;
		
		ArrayList<int[]> numbers = game.getBoard();
		
		for (int i = 0; i < 9; i++){
			int[] array = numbers.get(i);
			
			for (int j = 0; j < 9; j++){
				int num = array[j];
				if (num != 0){
					JLabel label = new JLabel(String.valueOf(num));
					label.setFont(new Font("Times New Roman", Font.BOLD, 20));
					label.setSize(label.getPreferredSize());
					label.setOpaque(false);
					label.setLocation(w * i + 15, h * j + 15);
					panel.add(label);
				} else {
					String[] nums = {"0","1","2","3","4","5","6","7","8","9"};
					JComboBox<String> label = new JComboBox<String>(nums);
					label.setSelectedIndex(0);
					label.addActionListener(new DropdownEvent(i, j, game));
					label.setFont(new Font("Times New Roman", Font.BOLD, 20));
					label.setSize(label.getPreferredSize());
					label.setOpaque(false);
					label.setLocation(w * i + 15, h * j + 15);
					panel.add(label);
				}
			}
		}
		frame.validate();
		frame.repaint();
	}

	private void loadImages() {
		try{
			_icon = ImageIO.read(Sudoku.class.getResource("/resources/logo.png"));
			_settingsBackground = ImageIO.read(Sudoku.class.getResource("/resources/settings_background.png"));
			_gameBackground = ImageIO.read(Sudoku.class.getResource("/resources/game_background.png"));
			_logo = ImageIO.read(Sudoku.class.getResource("/resources/icon.png"));
		} catch (IOException e){
			e.printStackTrace();
		} catch (IllegalArgumentException e){
			e.printStackTrace();
		}
		
	}
	
	public static int getDifficulty() {
		return _difficulty ;
	}
	
	public static void setDifficulty(int i) {
		_difficulty = i;
	}

}
