package com.klaschinger.suduko;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;

public class DifficultyEvent implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		
		JRadioButton button = (JRadioButton) e.getSource();
		
		if (button.getText().equals("Easy")){
			Sudoku.setDifficulty(40);
		} else if (button.getText().equals("Medium")){
			Sudoku.setDifficulty(45);
		} else if (button.getText().equals("Hard")){
			Sudoku.setDifficulty(50);
		}
		
		System.out.println(Sudoku.getDifficulty());
	}

}
