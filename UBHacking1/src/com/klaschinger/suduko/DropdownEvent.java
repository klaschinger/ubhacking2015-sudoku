package com.klaschinger.suduko;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

public class DropdownEvent implements ActionListener {

	private int _x;
	private int _y;
	private Game _game;
	
	public DropdownEvent(int x, int y, Game game){
		_x = x;
		_y = y;
		_game = game;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		JComboBox<String> button = (JComboBox<String>) e.getSource();
		
		_game.changeNum(_x, _y, (String) button.getSelectedItem());

	}

}
